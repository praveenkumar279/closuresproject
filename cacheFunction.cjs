function cacheFunction(callBack){
    if(typeof callBack !== 'function'){
        throw Error("Required an argument");
    }
    let cache = {};
    return function innerCacheFunction(...args){
        let passedArguments = args.join(',');
        if(!(passedArguments in cache)){
            cache[passedArguments] = callBack(...args);
            return cache[passedArguments];
        }
        return cache[passedArguments];
    }
}

module.exports = cacheFunction;