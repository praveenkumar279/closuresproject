function limitFunctionCallCount(callBack,number){
    if(typeof callBack !== 'function' || typeof number === 'undefined'){
        throw Error('Required callBack or number parameter');
    }

    return function innerLimitFunction(...args){
        if(number > 0){
            number--;
            return callBack(...args);
        }
        return null;
    }
}

module.exports = limitFunctionCallCount;