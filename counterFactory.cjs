function counterFactory(counterVariable){
    if(!counterVariable){
        counterVariable = 0;
    }
    return {
        increment : function(){
            counterVariable++;
            return counterVariable;
        },
        decrement : function(){
            counterVariable--;
            return counterVariable;
        }
    };
}



module.exports = counterFactory;