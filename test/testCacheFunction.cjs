
const cacheFunction = require('../cacheFunction.cjs');

function callBack(...args){
    return "callBack invoked";
}

const resultCacheFunction = cacheFunction(callBack);
console.log(resultCacheFunction(5,6));
console.log(resultCacheFunction(7));
console.log(resultCacheFunction(2));
console.log(resultCacheFunction(2));




module.exports = callBack;